import xml.etree.ElementTree as ET
import configparser
import QualysAPI
from countAssetGroupIPs import count_range


def getAssetGroups(api: QualysAPI.QualysAPI):
    fullurl = "%s/api/2.0/fo/asset/group/?action=list&show_attributes=TITLE,IP_SET" % api.server
    resp = api.makeCall(url=fullurl)
    return resp


def countIPSet(ipset: ET.Element):
    localcount = 0
    try:
        iprange = ipset.findall('.//IPRANGE')
    except AttributeError:
        pass
    else:
        for r in ipset.findall('.//IP_RANGE'):
            rtxt = r.text
            lower, upper = rtxt.split('-')
            localcount += count_range(lower=lower, upper=upper)

    try:
        singleips = ag.findall('.//IP')
    except AttributeError:
        pass
    else:
        localcount += len(singleips)

    return localcount


def getAGAssets(agTitle: str, api: QualysAPI.QualysAPI):
    fullurl = '%s/qps/rest/2.0/count/am/asset' % api.server
    payload = '<ServiceRequest>' \
              '<filters>' \
              '<Criteria field="tagName" operator="EQUALS">' \
              '%s' \
              '</Criteria></filters></ServiceRequest>' % agTitle
    resp = api.makeCall(url=fullurl, payload=payload)
    try:
        count = int(resp.find('.//count').text)
    except AttributeError:
        print("EnumerateAssetGroups: Could not identify asset count")
        ET.tostring(ag, encoding='utf-8', method='xml')
        return 0
    return count


def processAssetGroup(ag: ET.Element, api: QualysAPI.QualysAPI):
    localcount = 0
    try:
        agname = ag.find('TITLE').text
    except AttributeError:
        print("EnumerateAssetGroups:processAssetGroup Exception Finding Title")
        return '', 0, 0
    try:
        localcount += countIPSet(ag.find('IP_SET'))
    except AttributeError:
        pass

    assetcount = getAGAssets(agname, api)
    return agname, localcount, assetcount


if __name__ == '__main__':

    configfile = 'config.ini'

    config = configparser.ConfigParser()
    config.read(configfile)

    apiserver = config['DEFAULT']['APIServer']
    apiuser = config['DEFAULT']['APIUser']
    apipass = config['DEFAULT']['APIPass']
    apiProxyEnable = False
    apiProxyAddress=''

    if config['DEFAULT']['APIProxyEnable'] == 'Yes' or config['DEFAULT']['APIProxyEnable'] == 'True':
        apiProxyEnable = True
        apiProxyAddress = config['DEFAULT']['APIProxyAddress']

    api = QualysAPI.QualysAPI(svr=apiserver, usr=apiuser,passwd=apipass, proxy=apiProxyAddress,
                              enableProxy=apiProxyEnable)

    ipcount = 0
    allassetcount = 0

    agxml: ET.Element
    agxml = getAssetGroups(api=api)
    for ag in agxml.findall('.//ASSET_GROUP'):
        name, agcount, assetcount = processAssetGroup(ag=ag, api=api)
        print('%s : %s IPs, %s Assets' % (name, agcount, assetcount))
        ipcount += int(agcount)
        allassetcount += int(assetcount)

    print('Total IPs : %s\nTotal Assets : %s' % (ipcount, allassetcount))

