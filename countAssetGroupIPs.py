import ipaddress


def count_range(lower: str, upper: str):
    ip1 = ipaddress.ip_address(lower)
    ip2 = ipaddress.ip_address(upper)

    if ip1 > ip2:
        return int(ip1)-int(ip2)+1
    elif ip2 > ip1:
        return int(ip2)-int(ip1)+1
    else:
        return 1

